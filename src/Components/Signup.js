import React, { useState } from 'react'
import '../Components/CSS/signup.css'
import { Link } from 'react-router-dom'
import {useNavigate} from 'react-router-dom'
import axios from 'axios'


const Signup = () => {
    const [fullname, setFullName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    
    const [valid, setValid] = useState(false)
    const [success, setSuccess] = useState(false)



    const history = useNavigate()


    const handleSubmit = async (e) => {
      if (fullname && email && password) {
        setValid(true)
        setTimeout(()=> {
          history('/login')
        }, 3000)
      }
      setSuccess(true)
      e.preventDefault()

      try {const res = await axios.post("http://localhost:8080/webapp/api/user/save", {
        fullName: fullname.toString(),
        email: email,
        password: password,
        id: Math.floor(Math.random()*10000)
      }) 
    console.log(res.data) } 
      
      catch(err)  {
        console.log(err)
      }
    }
  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className='form-container'>
        <h1 className='header-text-signup'>Sign up</h1>
        {valid && success? <p className='signup-msg'>Successfully signed up</p> : null}
        <label>Full Name</label> <br></br>
        <input type='text' className='field' placeholder='Your name...' onChange={(e) => setFullName(e.target.value)} value = {fullname}/> <br></br>
        {success && !fullname ? <spam className = 'message-spam'>Please fill out full name</spam> : null}
        <label>E-mail</label> <br></br>
        <input type='email' className='field' placeholder='Your email...' onChange={(e) => setEmail(e.target.value)} value = {email}/> <br></br>
        {success && !email ? <spam className = 'message-spam'>Please fill out email</spam> : null}
        <label>Password</label>  <br></br>
        <input type='password' className='field' placeholder='Your password...' onChange={(e) => setPassword(e.target.value)} value = {password}/> <br></br>
        <input className='submit' type = 'submit' value = 'Sign up' />
        <spam className='signup-spam'>Already have an account? Log in<Link className='here' to='/login'> here.</Link></spam>
        </div>
      </form>
      <div className='bg'></div>
    </>
  )
}

export default Signup

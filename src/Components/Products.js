import React from 'react';
import IndividualProduct from "./IndividualProduct.js"



const Products = ({products}) => {
  
  return products.map((individualProduct) => (   
    <IndividualProduct key = {individualProduct.id} individualProduct = {individualProduct}/>
  ))
  
}

export default Products

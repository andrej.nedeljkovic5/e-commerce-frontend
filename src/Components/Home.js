import React, { useEffect, useState } from 'react'
import Products from './Products'
import axios from 'axios'
import Navigation from './Navigation'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export const CounterContext = React.createContext()
export const SetCounterContext = React.createContext()

const Home = () => {
  
  const [products, setProducts] =  useState([])
  
  const [counter, setCounter] = useState(0)
  

  const getProducts = async () => {
   try { const products = await axios.get("http://localhost:8080/webapp/api/product/findall")
   setProducts(products.data)
  
  } catch(e){
    console.log(e)
  }
}
useEffect(() => {
  getProducts()
},[])
  return (
    
    <div>
      <SetCounterContext.Provider value={setCounter}>
      <CounterContext.Provider value={counter}>
      <Navigation/>
      <Container className='cont-prod'>
        <Row>
          <Col><Products products = {products}/></Col>
        </Row>
      </Container>
      </CounterContext.Provider>
      </SetCounterContext.Provider>
    </div>
  )
}

export default Home

import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import '../Components/CSS/signup.css'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

const Login = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const [valid, setValid] = useState(false)

    const [fail, setFail] = useState(false)

    const [users, setUsers] = useState([])

    const navigate = useNavigate()

    useEffect(() => {
      axios.get('http://localhost:8080/webapp/api/user/findall')
      .then(res => {
        setUsers(res.data)
      })
    }, []) 

    const handleSubmit = (e) => {
      const user = users.find((user) => user.email === email && user.password === password)
      console.log(users)
      // here users are found via the find method from the JSON database
      if (user) {
        setValid(true)
        setTimeout(() => {
          navigate('/')
        },3000)
      } else {
        setFail(true)
        setTimeout(() => {
          setFail(false)
        }, 3000)
        
      }
      e.preventDefault()
    }


  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className='form-container-login'>
        <h1 className='header-text-signup'>Log in</h1>
        {valid? <p className='signup-msg'>Successfully Logged in</p> : null}
        {fail?  <p className='fail-signup-msg'>No user with those paramaters</p> : null}
        <label>E-mail</label> <br></br>
        <input type='email' className='field' onChange={(e) => setEmail(e.target.value)} value = {email}/> <br></br>
        <label>Password</label> <br></br>
        <input type='password' className='field' onChange={(e) => setPassword(e.target.value)} value = {password}/> <br></br>
        <input className='submit' type = 'submit' value = 'Sign up' />
        <spam className = 'login-spam'>Don't have an account? Sign up<Link className='here' to='/signup'> here.</Link></spam>
        </div>
      </form>
      <div className='bg'></div>
    </>
  )
}

export default Login



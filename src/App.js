import {BrowserRouter, Routes, Route} from 'react-router-dom'
import React from "react";
import Home from './Components/Home';
import Signup from './Components/Signup';
import Login from './Components/Login';
import Admin from './Components/Admin/Admin'

function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route exact path = '/' element = {<Home/>}/>
      <Route path = '/signup' element = {<Signup/>}/>
      <Route path = '/login' element = {<Login/>}/>
      <Route path = '/admin' element = {<Admin/>}/>
    </Routes>
    </BrowserRouter>
  );
}

export default App;

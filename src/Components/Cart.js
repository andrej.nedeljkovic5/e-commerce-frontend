import React, { useContext } from 'react'
import { CounterContext } from './Home'
import "./CSS/signup.css"
import { SetCounterContext } from './Home';
import Button from 'react-bootstrap/Button';


const Cart = () => {
  const setCounter = useContext(SetCounterContext)
  const counter = useContext(CounterContext)
  let button = null

  if (counter > 0) {
    button = <Button onClick={() => setCounter(counter - 1)}size="sm" variant="danger" type="submit">Remove</Button>
  }
  return (
    <div>
        <h4 className='add-cart'>{counter}</h4>
        {button}
    </div>
  )
}

export default Cart

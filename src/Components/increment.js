var increment = (function(n) {
    return function() {
      n += 1;
      return n;
    }
  }(6));

  export default increment
import axios from 'axios'
import React, { useState } from 'react'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';



const RemoveProduct = ({products}) => {
    
    const [id, findId] = useState('')
    const [success , setSucces] = useState(false)
    const [fail, setFail] = useState(false)

    
    const deleteProduct = async (e) => {
      e.preventDefault()
       const findid = products.find(product => product.id === id)
       console.log(findid)
       if (findid) {
       await axios.delete(`http://localhost:8080/webapp/api/product/delete/${id}`)
        setSucces(true)
        setTimeout(() =>{
            setSucces(false)
        }, 3000)
        .catch(e => console.log(e))
       } else {
        setFail(true)
        setTimeout(() => {
          setFail(false)
        }, 3000)
       }
    }

  return (
    <div className='admin-list'>
      <Form onSubmit={deleteProduct} className = "remove-product-container">
      <h3>Delete Product</h3>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label className='for-label'>Enter product ID to delete</Form.Label>
          <Form.Control type="text" onChange={(e) => findId(parseInt(e.target.value))}/>
        </Form.Group>
        <Button variant="primary" type="submit">Remove Product</Button>
        <ul>
        {success && <p>Product deleted</p>}
        {fail && <p>No product with that ID</p>}
      </ul>
      </Form>
    </div>
  )
}

export default RemoveProduct

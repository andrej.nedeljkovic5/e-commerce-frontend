import React from 'react'
import AddProduct from '../Admin/AddProduct'
import '../CSS/admin.css'
import RemoveProduct from '../Admin/RemoveProduct'
import { useState, useEffect } from 'react'
import axios from 'axios'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ProductList from './ProductList'
import UserList from './UserList'

const Admin = () => {

  const [products, setProducts] =  useState([])
  const [users, setUsers] = useState([])

  const getProducts = async () => {
     try { const products = await axios.get("http://localhost:8080/webapp/api/product/findall")
           const users = await axios.get('http://localhost:8080/webapp/api/user/findall')
    setProducts(products.data)
    setUsers(users.data)
   } catch(e){
     console.log(e)
   }
 }
 useEffect(() => {
   getProducts()
 },[])
  return (
    <Container className='admin-constainer'>
      <Row>
        <Col>
          <AddProduct/>
          <UserList users = {users}/>
        </Col>
        <Col>
          <RemoveProduct products = {products}/>
          <ProductList products = {products}/>
        </Col>
      </Row>
    </Container>
    
  )
}

export default Admin

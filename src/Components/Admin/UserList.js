import React from 'react'
import { ListGroup } from 'react-bootstrap'

const UserList = ({users}) => {
  return (
    <div>
        {users.map((user) => <ListGroup key={user.id} ><ListGroup.Item>{user.fullName}  {user.id}</ListGroup.Item></ListGroup>)}
    </div>
  )
}

export default UserList

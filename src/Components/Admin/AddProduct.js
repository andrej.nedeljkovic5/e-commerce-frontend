import axios from 'axios'
import React, { useState } from 'react'
import increment from '../increment'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

const AddProduct = () => {

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')


    const handleSubmit = async (e) => {
        e.preventDefault()
        console.log(name, description, price)
        try {
            const res = await axios.post("http://localhost:8080/webapp/api/product/save", {
                name: name,
                description: description,
                price: parseInt(price),
                id: increment(0)
            })
            console.log(res.data)
        } catch(e) {
            console.log(console.log(e))
        }
        e.target.value = null 

    }
  return (
    <div className='admin-list'>
      <Form onSubmit={handleSubmit}>
        <h3>Add Product</h3>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label className='for-label'>Name</Form.Label>
        <Form.Control type="text" onChange={(e) => setName(e.target.value)} value = {name}/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label className='for-label'>Description</Form.Label>
        <Form.Control type="text" onChange={(e) => setDescription(e.target.value)} value = {description}/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label className='for-label'>Price</Form.Label>
        <Form.Control type="text" onChange={(e) => setPrice(e.target.value)} value = {price}/>
      </Form.Group>
      <Button variant="primary" type="submit">Add Product</Button>
      </Form>
      
    </div>
  )
}

export default AddProduct

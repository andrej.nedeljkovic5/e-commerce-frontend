import React from 'react'
import ListGroup from 'react-bootstrap/ListGroup';

const ProductList = ({products}) => {
  return (
    <div>
        {products.map((product) => <ListGroup key={product.id} ><ListGroup.Item>{product.name} {product.id}</ListGroup.Item></ListGroup>)}
        <h5>Product list & their ID</h5>
    </div>
  )
}

export default ProductList

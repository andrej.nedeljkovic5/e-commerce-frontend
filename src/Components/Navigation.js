import React from 'react'
import { Navbar, Container, Nav } from 'react-bootstrap'
import Cart from './Cart'

const Navigation = () => {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/">E-Comm</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="/login">Login</Nav.Link>
            <Nav.Link href="/signup">Signup</Nav.Link>
            <Cart/>
          </Nav>
        </Container>
      </Navbar>
    </>
  )
}

export default Navigation

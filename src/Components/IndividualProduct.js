import React, { useContext } from 'react'
import "../Components/CSS/product.css"
import image from "../Components/images/iphone.png"
import Button from 'react-bootstrap/Button';
import { SetCounterContext } from './Home';
import { CounterContext } from './Home';


const IndividualProduct = ({individualProduct}) => {
   const setCounter = useContext(SetCounterContext)
   const counter = useContext(CounterContext)
   console.log(counter)
  return (
    <div className='product-container'>
        <div className='img-container'>
            <img className='product-image' src={image} alt='slika'/>
        </div>
        <div className='name'>{individualProduct.name}</div>
        <div className='description'>{individualProduct.description}</div>
        <div className='price'>{individualProduct.price}</div>
        <Button onClick={() => setCounter(counter + 1)} className = "add-to-cart" size="sm" variant="success" type="submit">Add</Button>
    </div>
  )
}

export default IndividualProduct
